grav = 2.5;
hsp = 0;
vsp = 0;
jumpspeed = 25;
movespeed = 10;
global.time = 0;
grounded = false;
jumping = true;
vel0 = true;

hsp_jump_constant_small = 5;
hsp_jump_constant_big = 10;
hsp_jump_applied = 0;

hkp_count = 0;
hkp_small = 2;
hkp_big = 5000;
