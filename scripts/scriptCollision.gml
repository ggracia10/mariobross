if (global.attack_key && !place_meeting(x,y,obj_fire)) {
    instance_destroy(other);
    audio_play_sound(attack_sound, 1, 0);
}
else if (!inmune){
    audio_play_sound(hit, 1, 0);
    global.vidas --;
    if (global.vidas <= 0) {
        var name = get_string("Put your name: ", "");
        highscore_add(name,score);
        audio_stop_sound(sound1);
        room_goto_next();
    }
    instance_destroy();
}
